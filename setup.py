from setuptools import setup, find_packages

setup(name='darkcast',
    description='A framework for recasting constraints from dark photon searches into other models',
    license='GPL2',
    packages=find_packages(),
    install_requires=[
        'matplotlib',
    ],
)

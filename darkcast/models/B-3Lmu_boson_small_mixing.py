# DARKCAST is licensed under the GNU GPL version 2 or later.
# Copyright (C) 2021 Philip Ilten, Yotam Soreq, Mike Williams, and Wei Xue.

from darkcast.pars import ge, mfs
# Define the fermion couplings.
xfs = {
    "e":      -1*ge*0.1,
    "mu":     -3-1*ge*0.1,
    "tau":    -1*ge*0.1,
    "nue":     0,
    "numu":   -3,
    "nutau":   0,
    "d":       1./3-1/3*ge*0.1,
    "u":       1./3+2/3*ge*0.1,
    "s":       1./3-1/3*ge*0.1,
    "c":       1./3+2/3*ge*0.1,
    "b":       1./3-1/3*ge*0.1,
    "t":       1./3+2/3*ge*0.1,
    }

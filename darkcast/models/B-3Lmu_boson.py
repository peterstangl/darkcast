# DARKCAST is licensed under the GNU GPL version 2 or later.
# Copyright (C) 2021 Philip Ilten, Yotam Soreq, Mike Williams, and Wei Xue.

from darkcast.pars import ge, mfs
# Define the fermion couplings.
xfs = {
    "e":      -1*ge,
    "mu":     -3-1*ge,
    "tau":    -1*ge,
    "nue":     0,
    "numu":   -3,
    "nutau":   0,
    "d":       1./3-1/3*ge,
    "u":       1./3+2/3*ge,
    "s":       1./3-1/3*ge,
    "c":       1./3+2/3*ge,
    "b":       1./3-1/3*ge,
    "t":       1./3+2/3*ge,
    }
